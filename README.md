# Git Workshop

This project is the raw material to support a demo about advanced Git practices:

* atomic and semantic commits with [Conventional Commits](https://www.conventionalcommits.org/),
* rewrite hstory with [git amend](https://www.atlassian.com/git/tutorials/rewriting-history) and [git rebase](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase),
* use [semantic-release](https://github.com/semantic-release/semantic-release) to automate the software release process.

## Demo

After introducting the required notions and tools:

1. start an MR from the issue _Parameterize Name_
2. implement it (💡 comply to conventional commits), but don't double quote variable => ShellCheck will fail
3. fix it, and `git commit --amend` (💡 atomicity)
4. also notice typo in the doc and fix it in a separate commit (probably not the best practice but it might happen) (💡 atomicity + comply to convential commits)
5. reviewer comments:
    * command help doesn't reflect the new option
    * new option not tested
6. take the comments into account, use `git rebase --interactive` to rewrite the history and maintain atomicity
7. merge the MR => a new release is performed
8. realize the new option is not documented in the README: commit (💡 comply to conventional commits) => no release is performed

---

## Universal Greetings Tool

Functionally, the project implements an awesome Universal Greetings toool (👈 omg a typo here 😱).

### Usage

```bash
./greetings.sh
```
