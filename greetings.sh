#!/usr/bin/env bash

set -e

function log_info() {
  >&2 echo -e "[INFO] $*"
}

function log_warn() {
  >&2 echo -e "[WARN] $*"
}

function log_error() {
  >&2 echo -e "[ERROR] $*"
}

function fail() {
  log_error "$@"
  exit 1
}

# parse arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case ${key} in
  -h|--help)
    echo "Usage: $0 [options]"
    echo
    echo "Options:"
    echo "       --help  Show help"
    exit 0
    ;;
  *)
    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

log_info Hello World
