#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"

@test "greetings works as expected" {
  # WHEN
  run ./greetings.sh
  # THEN
  assert_line --regexp ".* Hello World"
}
