#!/usr/bin/env bash

set -e

function log_info() {
  >&2 echo -e "[\\e[1;94mINFO\\e[0m] $*"
}

function log_warn() {
  >&2 echo -e "[\\e[1;93mWARN\\e[0m] $*"
}

function log_error() {
  >&2 echo -e "[\\e[1;91mERROR\\e[0m] $*"
}

function fail() {
  log_error "$@"
  exit 1
}

PROJECT_ID=${PROJECT_ID:-pismy%2Fgit-workshop}
GITLAB_API=${GITLAB_API:-https://gitlab.com/api/v4}

log_info "Reset Tags"
# curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API/projects/$PROJECT_ID/repository/tags" | jq -c '.[]' | while read tag
# do
#   echo $tag | jq -r '.name'
# done
curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API/projects/$PROJECT_ID/repository/tags?per_page=100" | jq -r '.[].name | select(.|test("^[0-9]+\\.[0-9]+\\.[0-9]+$"))' | while read -r tag_name
do
  if [[ "$tag_name" != "1.0.0" ]]
  then
    log_info "Delete tag $tag_name..."
    curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -X DELETE "$GITLAB_API/projects/$PROJECT_ID/repository/tags/$tag_name"
  fi
done

log_info "Reset MRs"
curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API/projects/$PROJECT_ID/merge_requests?per_page=100&state=all" | jq -r '.[].iid' | while read -r mr_iid
do
  log_info "Delete MR $mr_iid..."
  curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -X DELETE "$GITLAB_API/projects/$PROJECT_ID/merge_requests/$mr_iid"
done

log_info "Reset Branches"
curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API/projects/$PROJECT_ID/repository/branches?per_page=100" | jq -r '.[].name' | while read -r branch_name
do
  if [[ "$branch_name" != "main" ]]
  then
    log_info "Delete branch $branch_name..."
    curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -X DELETE "$GITLAB_API/projects/$PROJECT_ID/repository/branches/$branch_name"
  fi
done

log_info "Reopen Closed Issues"
curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API/projects/$PROJECT_ID/issues?per_page=100&state=closed" | jq -r '.[].iid' | while read -r issue_iid
do
  log_info "Reopen issue $issue_iid..."
  curl -sSf -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -X PUT "$GITLAB_API/projects/$PROJECT_ID/issues/$issue_iid?state_event=reopen"
done

log_info "Reset Repository"
log_info "TODO"
