# Developers Good Stuff

## Git config & aliases

My `$HOME/.gitconfig` content:

```
[init]
    defaultBranch = main
[user]
    name = Pierre Smeyers
    email = pierre.smeyers@gmail.com
[alias]
    co = checkout
    st = status -s
    ci = commit
    ca = commit --amend --no-edit
    br = branch
    df = diff
    pf = push --force-with-lease
    who = shortlog -s --
    lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset' --abbrev-commit --date=relative
[merge]
    tool = kdiff3
[diff]
    guitool = kdiff3
[credential]
    helper = cache --timeout=3600
[http]
    #	proxy = 10.236.240.71:3128
    #	proxy = localhost:3128
    #	sslVerify = false
    postBuffer = 524288000
[push]
    default = simple
    autoSetupRemote = true
[pull]
    rebase = true
[core]
    autocrlf = input
    ignorecase = false
    pager = cat
[filter "lfs"]
    clean = git-lfs clean -- %f
    smudge = git-lfs smudge -- %f
    process = git-lfs filter-process
    required = true
```